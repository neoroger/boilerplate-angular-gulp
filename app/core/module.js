

var app = angular.module('app', [
    'ui.router',        // Router con estados. Usamos ui-view, ui-sref y ui-sref-active.
    'ui.bootstrap',     // Nos da variables para crear modales y demás componentes bootstrap.
    'angular-storage',  // Guarda variables en el almacenamiento local del navegador.
    'ngAnimate'         // Añade clases en ng-if, ng-show, etc. para poder usar animate.css y demás.
]);