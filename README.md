# README #

Boilerplate con Angular y Gulp.

### Instalación ###

```
#!bash

sudo npm install
bower install
gulp prepare-bower
gulp serve

```


### Librerías bower preinstaladas ###

* jquery
* bootstrap
* underscore
* angular
* angular-animate
* angular-bootstrap
* angular-ui-router
* a0-angular-storage
* animate.css
* hover